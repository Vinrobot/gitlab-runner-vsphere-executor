# GitLab Runner - vSphere Executor

GitLab runner custom executor for vSphere.

## Configuration

The `GOVC_*` must be used to configure `govc` to connect to vSphere.
Official documentation: https://github.com/vmware/govmomi/blob/813c2dcb4ed543e6ce796647368bb69f059f852d/govc/USAGE.md

The scripts use the following variables:
- `VC_TEMPLATE`: Virtual machine to clone (`govc vm.clone`)
- `VC_SNAPSHOT`: Snapshot name to clone from (`govc vm.clone`)
- `VC_CPUS`: Number of CPUs (`govc vm.clone`)
- `VC_MEMS`: Size in MB of memory (`govc vm.clone`)
- `VC_SHELL`: Shell to use in the virtual machine
- `VC_SSH_USER`: User used to connect via SSH
- `VC_SSH_PASSWORD` (optional): User's password
- `VC_SSH_KEY_PATH` (optional): Path to a SSH key

Example:
```properties
# Used by GOVC
GOVC_INSECURE=1
GOVC_URL=vcsa.homelab.local
GOVC_USERNAME=administrator@vsphere.local
GOVC_PASSWORD=********
GOVC_DATACENTER=Datacenter
GOVC_DATASTORE=datastore1
GOVC_FOLDER=Runners
GOVC_HOST=macmini1
GOVC_NETWORK=LAN
GOVC_RESOURCE_POOL=Resources

# Used by the scripts
VC_TEMPLATE=GitLab-Runner-macOS-Template
VC_SNAPSHOT=Snapshot
VC_CPUS=4
VC_MEMS=8192
VC_SSH_USER=gitlab
VC_SSH_PASSWORD=gitlab
VC_SHELL=/bin/bash
```
