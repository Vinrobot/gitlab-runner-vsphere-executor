#!/usr/bin/env bash
set -eo pipefail

# Trap any error, and mark it as a system failure.
trap "exit $SYSTEM_FAILURE_EXIT_CODE" ERR

# Destroy VM.
govc vm.destroy "$VM_ID"
