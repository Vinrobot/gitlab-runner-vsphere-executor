_get_vm_ip() {
    local IPV4_REGEX='[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}'
    local JQ_PATH='.VirtualMachines[].Guest.Net[].IpConfig.IpAddress[].IpAddress'

    JSON=$(govc vm.info -json "$VM_ID")
    if [ $? -ne 0 ]; then
        echo ""
    else
        echo "$JSON" | (jq -r "$JQ_PATH" 2>/dev/null || true) | grep -E "$IPV4_REGEX" | sort -u | head -n1
    fi
}

_connect_ssh() {
    if [ -n "$VC_SSH_KEY_PATH" ]; then
        # Connect with SSH Key if present
        ssh -i "$VC_SSH_KEY_PATH" -o StrictHostKeyChecking=no "$@" "$VC_SSH_USER@$VM_IP" "$VC_SHELL"
    elif [ -n "$VC_SSH_PASSWORD" ]; then
        # Connect with Password if present
        sshpass -p "$VC_SSH_PASSWORD" ssh -o StrictHostKeyChecking=no "$@" "$VC_SSH_USER@$VM_IP" "$VC_SHELL"
    else
        ssh -o StrictHostKeyChecking=no "$@" "$VC_SSH_USER@$VM_IP" "$VC_SHELL"
    fi
}
