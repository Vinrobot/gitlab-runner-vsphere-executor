#!/usr/bin/env bash
set -eo pipefail

# Trap any error, and mark it as a system failure.
trap "exit $SYSTEM_FAILURE_EXIT_CODE" ERR

# Load functions
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
source "${SCRIPT_DIR}/00-functions.sh"

# Clone VM
govc vm.clone -on=true -waitip=true -link=true \
    "-vm=$VC_TEMPLATE" \
    "-snapshot=$VC_SNAPSHOT" \
    "-c=$VC_CPUS" \
    "-m=$VC_MEMS" \
    "$VM_ID"

# Wait for IP
echo 'Waiting for the VM to get an IP'
for i in {1..30}; do
    VM_IP="$(_get_vm_ip)"
    if [ -n "$VM_IP" ]; then
        echo "VM got IP: $VM_IP"
        break
    fi

    if [ "$i" == "30" ]; then
        echo 'Waited 30 seconds for VM to start, exiting...'
        exit "$SYSTEM_FAILURE_EXIT_CODE"
    fi

    sleep 1s
done

# Wait for SSH
echo 'Waiting for SSH to be available'
for i in {1..30}; do
    if _connect_ssh >/dev/null 2>/dev/null; then
        break
    fi

    if [ "$i" == "30" ]; then
        echo 'Waited 30 seconds for SSH to be available, exiting...'
        exit "$SYSTEM_FAILURE_EXIT_CODE"
    fi

    sleep 1s
done
