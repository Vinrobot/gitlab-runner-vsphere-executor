#!/usr/bin/env bash
set -eo pipefail

# Trap any error, and mark it as a system failure.
trap "exit $SYSTEM_FAILURE_EXIT_CODE" ERR

VM_ID="runner-$CUSTOM_ENV_CI_RUNNER_ID-project-$CUSTOM_ENV_CI_PROJECT_ID-concurrent-$CUSTOM_ENV_CI_CONCURRENT_PROJECT_ID-job-$CUSTOM_ENV_CI_JOB_ID"

cat << EOS
{
  "driver": {
    "name": "vSphere",
    "version": "v0.0.1"
  },
  "job_env" : {
    "VM_ID": "$VM_ID"
  }
}
EOS
