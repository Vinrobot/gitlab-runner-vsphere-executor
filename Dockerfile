FROM vmware/govc as govc

FROM gitlab/gitlab-runner

RUN set -ex \
	&& apt update \
	&& apt -y install \
		jq \
		sshpass \
	&& rm -rf /var/lib/apt/lists/*

COPY --from=govc /govc /usr/local/bin/govc

COPY rootfs/ /
