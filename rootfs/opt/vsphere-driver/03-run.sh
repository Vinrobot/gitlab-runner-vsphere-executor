#!/usr/bin/env bash
set -eo pipefail

# Trap any error, and mark it as a system failure.
trap "exit $SYSTEM_FAILURE_EXIT_CODE" ERR

# Load functions
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
source "${SCRIPT_DIR}/00-functions.sh"

# Wait for IP
for i in {1..30}; do
    VM_IP="$(_get_vm_ip)"
    if [ -n "$VM_IP" ]; then
        break
    fi

    if [ "$i" == "30" ]; then
        echo 'Waited 30 seconds for VM to start, exiting...'
        exit "$SYSTEM_FAILURE_EXIT_CODE"
    fi

    sleep 1s
done

# Run command
if ! _connect_ssh -T < "${1}"; then
    exit "$BUILD_FAILURE_EXIT_CODE"
fi
